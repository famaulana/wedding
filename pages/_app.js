import 'bootstrap/dist/css/bootstrap.css';
import '../styles/globals.css'
import '../styles/responsive.css'

import { wrapper } from '../redux/store';
import { Provider, useStore } from "react-redux";

import { PersistGate } from 'redux-persist/integration/react'

const MyApp = ({ Component, pageProps }) => {

  const store = useStore((state) => state)

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={store.__persistor}>
        <Component {...pageProps} />
      </PersistGate>
    </Provider>
  );
};

export default wrapper.withRedux(MyApp);

import { postMethod } from "../../utils/repository";

export default async (request, response) => {

  const path = 'contents'
  const endpoint = process.env.BASE_URL_API + path;
  const data = {
    type: request.body?.type,
  };

  const auth = {
    baun: process.env.BASIC_AUTH_API?.capiaus,
    baup: process.env.BASIC_AUTH_API?.capiaup
  };

  var getPackage = await postMethod(endpoint, data, auth)
    .then(function handledResolved(response) {
      return response
    })
    .catch(async function (err) {
      resolve({ code: err.response.status, info: err.response.statusText, data: null })
    });

  return response.json(getPackage)
}
import Head from 'next/head'
import Image from 'next/image'
import AOS from 'aos'
import 'aos/dist/aos.css';
import styles from './index.module.scss'

import React, { useEffect, useState } from 'react';

export default function Home() {

  const [toggleProfile, setToggleProfile] = useState(1)
  const [days, setDays] = useState()
  const [hours, setHours] = useState()
  const [minutes, setMinutes] = useState()
  const [seconds, setSeconds] = useState()

  useEffect(() => {
    AOS.init();
  }, [])

  setInterval(() => {
    let time = Date.parse(new Date("2022-11-27T19:00:00Z")) - Date.parse(new Date())
    const seconds = Math.floor((time / 1000) % 60);
    const minutes = Math.floor((time / 1000 / 60) % 60);
    const hours = Math.floor((time / (1000 * 60 * 60)) % 24);
    const days = Math.floor(time / (1000 * 60 * 60 * 24));
    setDays(seconds)
    setHours(minutes)
    setMinutes(hours)
    setSeconds(days)
  }, 1000)

  setInterval(() => {
    setToggleProfile(toggleProfile === 1 ? 2 : 1)
  }, 8000)

  return (
    <>
      <Head>
        <title>The Wedding of Fardina & Zion</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons|Material+Icons+Outlined" rel="stylesheet" />
      </Head>

      <div className={styles.canvas}>

        <section className={styles.cover}>
          <div className={styles.containerImage}>
            <img src={'http://179.61.188.76/banner.jpg'} fill alt='cover photo'/>
          </div>
          <div className={styles.containerText}>
            <p>The Wedding</p>
            <h3>Fardina & Zion</h3>
            <p>27 - 11 - 2022</p>

            <div className={styles.containerCountdown}>
              <div className={styles.days}>
                <p>{seconds}</p>
                <p>Days</p>
              </div>
              <div className={styles.hours}>
                <p>{minutes}</p>
                <p>Hours</p>
              </div>
              <div className={styles.minutes}>
                <p>{hours}</p>
                <p>Minutes</p>
              </div>
              <div className={styles.seconds}>
                <p>{days}</p>
                <p>Seconds</p>
              </div>
            </div>
          </div>
        </section>

        <section className={styles.hadist}>
          <h3 data-aos="fade-up">F & Z</h3>
          <p data-aos="fade-up">QS. Ar Rum 30:21</p>
          <p data-aos="fade-up">Dan di antara tanda-tanda kekuasaan-Nya ialah Dia menciptakan untukmu istri-istri dari jenismu sendiri,supaya kamu cenderung dan merasa tenteram kepadanya, dan dijadikan-Nya di antaramu rasa kasih sayang. Sesungguhnya pada yang demikian itu benar-benar terdapat tanda-tanda bagi kaum yang berpikir</p>
        </section>

        <section className={styles.profile}>
          <div className={styles.containerImage} data-aos="fade-right">
            <img className={''} src={'http://179.61.188.76/LRM_EXPORT_20221118_184908.jpg'} fill alt='Bridge'/>
            {/* <img className={toggleProfile === 2 ? styles.active : ''} src={'http://179.61.188.76/DSC08929.JPG'} fill alt='Bridge'/> */}
          </div>
          <div className={styles.textleft} data-aos="fade-up">
            <p>Fardina Aulia</p>
            <p>Putri Kedua dari : </p>
            <p>Bapak Achmad Tamami & Ibu Umi Nadziroh</p>
          </div>
          <div className={styles.containerImage} data-aos="fade-left">
            <img className={''} src={'http://179.61.188.76/LRM_EXPORT_20221118_184816.jpg'} fill alt='Bridge'/>
            {/* <img className={toggleProfile === 2 ? styles.active : ''} src={'http://179.61.188.76/DSC08968.JPG'} fill alt='Bridge'/> */}
          </div>
          <div className={styles.textright} data-aos="fade-up">
            <p>Zion Mahardikara</p>
            <p>Putra Kedua dari : </p>
            <p>Bapak H. Didik Susilo & Ibu Hj. Melas Hariasih</p>
          </div>
        </section>

        <section className={styles.event}>
          <div className={styles.heading} data-aos="fade-right">
            <p>The</p>
            <p>Wedding</p>
          </div>

          <div className={styles.ticket} data-aos="fade-left">
            <div className={styles.left}>
              <h3>Akad Nikah</h3>
            </div>
            <div className={styles.right}>
              <div className={styles.date}>
                <p>27</p>
                <p>Ahad, November 2022</p>
              </div>

              <p className={styles.time}><i className="material-icons">query_builder</i>09.00 WIB - 11.00 WIB</p>
              <p>Lokasi Acara</p>
              <p><b>Rumah Bapak Achmad Tamami</b></p>
              <p>Dsn. Sukopuro RT.02/RW.01, Ds. Sukonatar, Kec. Srono, Kab. Banyuwangi, Jawa Timur</p>
              <a href="https://goo.gl/maps/Cfv8hiYqoREFaQso6" target="_blank">Google Maps <i className='material-icons-outlined'>place</i></a>
            </div>
          </div>

          <div className={styles.ticket} data-aos="fade-right">
            <div className={styles.left}>
              <h3>Resepsi</h3>
            </div>
            <div className={styles.right}>
              <div className={styles.date}>
                <p>27</p>
                <p>Ahad, November 2022</p>
              </div>

              <p className={styles.time}><i className="material-icons">query_builder</i>11.00 WIB - 13.00 WIB</p>
              <p>Lokasi Acara</p>
              <p><b>Rumah Bapak Achmad Tamami</b></p>
              <p>Dsn. Sukopuro RT.02/RW.01, Ds. Sukonatar, Kec. Srono, Kab. Banyuwangi, Jawa Timur</p>
              <a href="https://goo.gl/maps/Cfv8hiYqoREFaQso6" target="_blank">Google Maps <i className='material-icons-outlined'>place</i></a>
            </div>
          </div>
        </section>

        <section className={styles.protocol}>
          <p>Mohon untuk mematuhi protokol</p>
          <div className={'container ' + styles.container}>
            <img src='../mask.png'/>
            <img src='../no-handshake.png'/>
            <img src='../washing-hands.png'/>
            <img src='../distance.png'/>
          </div>
        </section>

        <section className={styles.footer}>
          <p data-aos="fade-up">Terima Kasih</p>
          <p data-aos="fade-up">Merupakan suatu kebahagiaan dan kehormatan bagi kami, apabila Bapak/Ibu/Saudara/i, berkenan hadir dan memberikan do’a restu kepada kami.</p>
          <p data-aos="fade-up">KAMI YANG BERBAHAGIA</p>
          <p data-aos="fade-up">Fardina & Zion</p>
        </section>

      </div>
    </>
  )
}

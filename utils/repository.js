import axios from 'axios'
import {
  isJson
} from './helper'

export function getLocalMethod(url) {
  let head = {}
  head["Content-Type"] = 'application/json'
  head["Access-Control-Allow-Origin"] = '*'

  return new Promise(function (resolve) {
    axios({
      url: url,
      method: 'GET',
      headers: head,
      withCredentials: true,
    })
      .then(async function (response) {
        if (isJson(response.data)) {
          resolve(response.data)
        } else {
          resolve({
            code: "-3",
            info: "Balikan API bermasalah.",
            data: response.data
          })
        }
      })
      .catch(async function (err) {
        resolve({
          code: err.response?.status,
          info: err.response?.statusText,
          data: null
        })
      });
  })
}

export function getMethod(url, auth) {
  let head = {}
  head["Content-Type"] = 'application/json'
  head["Access-Control-Allow-Origin"] = '*'

  return new Promise(function (resolve) {
    axios({
      url: url,
      method: 'GET',
      headers: head,
      withCredentials: true,
      auth: {
        username: auth?.baun,
        password: auth?.baup
      }
    })
      .then(async function (response) {
        if (isJson(response.data)) {
          resolve(response.data)
        } else {
          resolve({
            code: "-3",
            info: "Balikan API bermasalah.",
            data: response.data
          })
        }
      })
      .catch(async function (err) {
        resolve({
          code: err.response?.status,
          info: err.response?.statusText,
          data: null
        })
      });
  })
}

export function postLocalMethod(url, params) {
  let head = {}
  head["Content-Type"] = 'application/json'

  return new Promise(function (resolve) {
    axios({
      url: url,
      method: 'POST',
      headers: head,
      withCredentials: true,
      data: params
    })
      .then(async function (response) {
        if (isJson(response.data)) {
          resolve(response.data)
        } else {
          resolve({
            code: "-3",
            info: "Balikan API bermasalah.",
            data: response.data
          })
        }
      })
      .catch(async function (err) {
        resolve({
          code: err.response?.status,
          info: err.response?.statusText,
          data: null
        })
      });
  })
}

export function postMethod(url, params, auth) {
  let head = {}
  head["Content-Type"] = 'application/json'

  return new Promise(function (resolve) {
    axios({
      url: url,
      method: 'POST',
      headers: head,
      withCredentials: true,
      auth: {
        username: auth?.baun,
        password: auth?.baup
      },
      data: params
    })
      .then(async function (response) {
        if (isJson(response.data)) {
          resolve(response.data)
        } else {
          resolve({
            code: "-3",
            info: "Balikan API bermasalah.",
            data: response.data
          })
        }
      })
      .catch(async function (err) {
        resolve({
          code: err.response?.status,
          info: err.response?.statusText,
          data: null
        })
      });
  })
}
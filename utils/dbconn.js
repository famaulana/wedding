export function getQuery(query, param) {

  var mysql = require('mysql');
  var start = Date.now();

  return new Promise(function (resolve) {
    var con = mysql.createConnection({
      host: process.env.DB_CONFIG.host,
      user: process.env.DB_CONFIG.user,
      password: process.env.DB_CONFIG.password,
      database: process.env.DB_CONFIG.database
    });

    con.connect(function (err) {
      if (err) {
        resolve({ code: "-1", info: "MYSQL Connection Error.", data: { err } })
      }
      else {
        var sql = query
        var inserts = param
        sql = mysql.format(sql, inserts)

        con.query(sql, function (err, result, fields) {
          if (err) {
            resolve({ code: "-1", info: "MYSQL Connection Error QUERY", data: { err } })
            con.end();
          }
          else {
            var millis = Date.now() - start;
            if (sql.substring(0, 6).toUpperCase() == "INSERT") {
              // resolve({ code: "0", info: sql, data: result })
              resolve({ code: "0", info: 'sukses', data: result, response_time: millis })
            } else if (sql.substring(0, 6).toUpperCase() == "UPDATE") {
              // resolve({ code: "0", info: sql, data: result })
              resolve({ code: "0", info: 'sukses', data: result, response_time: millis })
            } else {
              // Kalo query select, di cek dulu length-nya, kalo gak ada data, balikkin gagal
              if (result.length > 0) {
                // resolve({ code: "0", info: sql, data: result })
                resolve({ code: "0", info: 'sukses', data: result, response_time: millis })
              }
              else {
                resolve({ code: "10", info: "Data tidak ditemukan", data: result, response_time: millis })
              }
            }
            con.end();
          }

        });
      }
    });
  })


}
import moment from 'moment'

export function isEmpty(str) {
  if (str == '' || str == undefined || str == null) {
    return true
  } else {
    return false
  }
}

export function isJson(str) {
  str = typeof str !== 'string' ? JSON.stringify(str) : str

  try {
    str = JSON.parse(str)
  } catch (e) {
    return false
  }

  if (typeof str === 'object' && str !== null) {
    return true
  }

  return false
}

export function isEmail(item) {
  if (new RegExp(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,15}/g).test(item)) {
    return true
  } else {
    return false
  }
}

export function jsonParse(item) {
  let data = ''
  try {
    if (isJson(item)) {
      data = JSON.parse(item)
    } else {
      data = item
    }
  } catch (e) {
    if (isJson(item)) {
      data = item
    }
  }

  return data
}

export function jsonRes(code, info, data) {
  return { code: code, info: info, data: data }
}

export function base64_encode(str) {
  let strnya = new Buffer(str)

  let encoded = strnya.toString('base64')

  return encoded
}

export function DateDifference(datetime) {
  var start = new Date(datetime.replace(/-/g, '/'))
  var now = new Date()

  var startTime = moment(start).format('YYYY-MM-DD HH:mm:ss')
  var endTime = moment(now).format('YYYY-MM-DD HH:mm:ss')

  startTime = moment(startTime, 'YYYY-MM-DD HH:mm:ss')
  endTime = moment(endTime, 'YYYY-MM-DD HH:mm:ss')

  // calculate total duration
  var duration = moment.duration(endTime.diff(startTime))

  // duration in hours
  var hours = endTime.diff(startTime, 'hours')

  // duration in minutes
  var minutes = moment.utc(endTime.diff(startTime)).format('mm')

  // duration in days
  var days = endTime.diff(startTime, 'days')

  // seconds in seconds
  var seconds = endTime.diff(startTime, 'seconds')

  if (days != 0) {
    return days + ' hari yang lalu'
  } else if (hours != 0) {
    return hours + ' jam yang lalu'
  } else if (minutes != 0) {
    if (minutes.substring(0, 1) == '0') {
      return minutes.substring(1) + ' menit yang lalu'
    } else {
      return minutes + ' menit yang lalu'
    }
  } else {
    return seconds + ' detik yang lalu'
  }
}

export function DateDifferenceInSeconds(datetime) {
  if (!isEmpty(datetime)) {
    var start = new Date(datetime.replace(/-/g, '/'))
    var now = new Date()

    var startTime = moment(start).format('YYYY-MM-DD HH:mm:ss')
    var endTime = moment(now).format('YYYY-MM-DD HH:mm:ss')

    startTime = moment(startTime, 'YYYY-MM-DD HH:mm:ss')
    endTime = moment(endTime, 'YYYY-MM-DD HH:mm:ss')

    // seconds in seconds
    var seconds = endTime.diff(startTime, 'seconds')

    return seconds
  } else {
    return false
  }
}

export function DateFull(datetime) {
  if (typeof datetime == 'undefined') {
    return ''
  }

  var d = new Date(datetime.replace(/-/g, '/'))

  var date = d.getDate()
  var month = d.getMonth() + 1 // Since getMonth() returns month from 0-11 not 1-12
  var year = d.getFullYear()

  switch (month) {
    case 1:
      month = 'Jan'
      break
    case 2:
      month = 'Feb'
      break
    case 3:
      month = 'Mar'
      break
    case 4:
      month = 'Apr'
      break
    case 5:
      month = 'May'
      break
    case 6:
      month = 'Jun'
      break
    case 7:
      month = 'Jul'
      break
    case 8:
      month = 'Aug'
      break
    case 9:
      month = 'Sep'
      break
    case 10:
      month = 'Oct'
      break
    case 11:
      month = 'Nov'
      break
    case 12:
      month = 'Dec'
      break

    default:
      break
  }

  return date + ' ' + month + ' ' + year
}

export function specialChars(text) {
  return text.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&#039;')
}

export function funcCurrency(str, prefix) {
  if (typeof str != 'undefined') {
    var separator = ''

    var number_string = str.toString().replace(/[^,\d]/g, ''),
      split = number_string.split('.'),
      sisa = split[0].length % 3,
      rupiah = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi)

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? '.' : ''
      rupiah += separator + ribuan.join('.')
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah
    return prefix == undefined ? rupiah : rupiah ? rupiah : ''
  } else {
    return ''
  }
}

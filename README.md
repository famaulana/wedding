Source code template dengan menggunakan bahasa Node JS dan framework [Next.js](https://nextjs.org/). Serta menggunakan React Bootstrap sebagai framework HTML dan CSS.

## Packages

Di bawah ini adalah daftar npm packages yang sudah terinstall:

```bash
eslint
prettier
react-bootstrap
axios
mysql
formik
yup
crypto-js
qs
moment
react-datepicker
eslint-config-airbnb
@reduxjs/toolkit
next-redux-wrapper
react-redux
redux-devtools-extension
redux-logger
redux-persist
redux-thunk
```

## Cara penggunaan

Ikuti step di bawah ini untuk menjalankan website:

```bash
Go to app directory

npm install
npm run dev
```

Buka [http://localhost:3000](http://localhost:3000) dengan browser kalian untuk melihat segala perubahan yang kalian buat.

Setelah selesai development, gunakan perintah di bawah ini sebelum melakukan push ke dalam repository:

```bash
npm run build
```

import { SAMPLES, DEAUTHENTICATE } from '../actionTypes'


export const sampleAct = (params) => {
  return (dispatch) => {
    dispatch({ type: SAMPLES, payload: { data: null } })
  }
}

export const deauthenticate = (params) => {
  return (dispatch) => {
    window.location.href = '/login'
    dispatch({ type: DEAUTHENTICATE })
  }
}
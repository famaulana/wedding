import { SAMPLES, DEAUTHENTICATE } from '../actionTypes'
import { HYDRATE } from 'next-redux-wrapper'

const initState = {
  sampleData: {},
}

const sampleReducer = (state = initState, action) => {
  switch (action.type) {
    case HYDRATE:
      return {
        ...state,
        ...action.payload,
      }
    case SAMPLES:
      return {
        ...state,
        sampleData: action.payload.data,
      }
    case DEAUTHENTICATE:
      return initState
    default:
      return state
  }
}

export default sampleReducer

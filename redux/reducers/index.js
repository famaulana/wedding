import { combineReducers } from "redux";
import storage from 'redux-persist/lib/storage'

import sampleReducer from "./sampleReducer";

const appReducer = combineReducers({
  sampleState: sampleReducer
})

// Handle logout
const rootReducer = (state, action) => {

  // Kalo logout
  if (action.type === 'DEAUTHENTICATE') {
    storage.removeItem('persist:root')
    state = undefined
  }

  return appReducer(state, action)
}

export default rootReducer;